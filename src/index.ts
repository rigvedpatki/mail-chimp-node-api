import Koa, { Context } from 'koa';
import Router from 'koa-router';
import bodyParser from 'koa-bodyparser';
import serve from 'koa-static';
import path from 'path';
import axios from 'axios';

const app = new Koa();

app.use(bodyParser());
app.use(serve(path.resolve(__dirname, './public')));

const router = new Router();

router.post('/signup', async (ctx: Context) => {
  const { firstName, lastName, email } = ctx.request.body;

  if (!firstName || !lastName || !email) {
    ctx.redirect('./fail.html');
    return;
  }

  try {
    const data = JSON.stringify({
      members: [
        {
          email_address: email,
          status: 'subscribed',
          merge_fields: {
            FNAME: firstName,
            LNAME: lastName
          }
        }
      ]
    });
    console.log(data);
    const response = await axios({
      baseURL: 'https://us19.api.mailchimp.com',
      url: '/3.0/lists/e7af3fb324',
      method: 'POST',
      headers: {
        Authorization: 'auth 7bfd0ea426d0817c1fe7216c67c83b24-us19',
        'Content-Type': 'application/json, text/plain'
      },
      data
    });

    if (response.status === 200) {
      ctx.redirect('./success.html');
    } else {
      ctx.redirect('./fail.html');
    }
  } catch (error) {
    ctx.redirect('./fail.html');
  }
});

app.use(router.routes());

const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
